package com.accenture.academico.service;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.accenture.academico.model.Pessoa;

public interface PessoaService {
	@Transactional
	public void addPessoa(Pessoa p);
	@Transactional
	public List<Pessoa> listPessoas();
	@Transactional
	public void updatePessoa(Pessoa p);
	@Transactional
	public void deletePessoa(Pessoa p);


}
