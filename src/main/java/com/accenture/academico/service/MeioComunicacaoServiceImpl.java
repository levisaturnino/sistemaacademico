package com.accenture.academico.service;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;

import com.accenture.academico.model.MeioComunicacao;
import com.accenture.academico.dao.MeioComunicacaoDAO;



@ManagedBean(name = "MeioComunicacaoService")
@SessionScoped
@Service

public class MeioComunicacaoServiceImpl implements MeioComunicacaoService {

	private MeioComunicacaoDAO MeioComunicacaoDAO;

	public void setMeioComunicacaoDAO(MeioComunicacaoDAO MeioComunicacaoDAO) {
		this.MeioComunicacaoDAO = MeioComunicacaoDAO;
	}

	@Override
	//@Transactional
	public void addMeioComunicacao(MeioComunicacao m) {
		// TODO Auto-generated method stub
		MeioComunicacaoDAO.addMeioComunicacao(m);		

	}

	@Override
	public List<MeioComunicacao> listMeiosComunicacao() {
		// TODO Auto-generated method stub
		return MeioComunicacaoDAO.listMeiosComunicacao();
	}

	@Override
	//@Transactional
	public void updateMeioComunicacao(MeioComunicacao m) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteMeioComunicacao(MeioComunicacao m) {
		// TODO Auto-generated method stub

	}

}
