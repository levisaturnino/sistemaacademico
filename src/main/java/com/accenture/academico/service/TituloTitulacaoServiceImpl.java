package com.accenture.academico.service;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;

import com.accenture.academico.model.TituloTitulacao;
import com.accenture.academico.dao.TituloTitulacaoDAO;



@ManagedBean(name = "TituloService")
@SessionScoped
@Service

public class TituloTitulacaoServiceImpl implements TituloTitulacaoService {

	private TituloTitulacaoDAO TituloDAO;

	public void setTituloDAO(TituloTitulacaoDAO TituloDAO) {
		this.TituloDAO = TituloDAO;
	}

	@Override
	//@Transactional
	public void addTitulo(TituloTitulacao t) {
		// TODO Auto-generated method stub
		TituloDAO.addTitulo(t);		

	}

	@Override
	public List<TituloTitulacao> listTitulos() {
		// TODO Auto-generated method stub
		return TituloDAO.listTitulos();
	}

	@Override
	//@Transactional
	public void updateTitulo(TituloTitulacao t) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteTitulo(TituloTitulacao t) {
		// TODO Auto-generated method stub

	}

}
