package com.accenture.academico.service;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Transactional;

import com.accenture.academico.dao.PessoaDAO;
import com.accenture.academico.model.Pessoa;



@ManagedBean(name = "pessoaService")
@SessionScoped
@Service
@Transactional
public class PessoaServiceImpl implements PessoaService {

	private PessoaDAO PessoaDAO;


	public void setPessoaDAO(PessoaDAO PessoaDAO) {
		this.PessoaDAO = PessoaDAO;
	}

	@Override
	@Transactional
	public void addPessoa(Pessoa p) {		
		
		PessoaDAO.addPessoa(p);		

	}

	@Override
	@Transactional
	public List<Pessoa> listPessoas() {
		// TODO Auto-generated method stub
		return PessoaDAO.listPessoas();
	}

	@Override
	@Transactional
	public void updatePessoa(Pessoa p) {
		// TODO Auto-generated method stub

	}

	@Override
	@Transactional
	public void deletePessoa(Pessoa p) {
		// TODO Auto-generated method stub

	}

}
