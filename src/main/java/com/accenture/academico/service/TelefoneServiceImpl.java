package com.accenture.academico.service;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;

import com.accenture.academico.model.Telefone;
import com.accenture.academico.dao.TelefoneDAO;



@ManagedBean(name = "TelefoneService")
@SessionScoped
@Service

public class TelefoneServiceImpl implements TelefoneService {

	private TelefoneDAO TelefoneDAO;

	public void setTelefoneDAO(TelefoneDAO TelefoneDAO) {
		this.TelefoneDAO = TelefoneDAO;
	}

	@Override
	//@Transactional
	public void addTelefone(Telefone t) {
		// TODO Auto-generated method stub
		TelefoneDAO.addTelefone(t);		

	}

	@Override
	public List<Telefone> listTelefones() {
		// TODO Auto-generated method stub
		return TelefoneDAO.listTelefones();
	}

	@Override
	//@Transactional
	public void updateTelefone(Telefone t) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteTelefone(Telefone t) {
		// TODO Auto-generated method stub

	}

}
