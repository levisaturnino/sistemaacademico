package com.accenture.academico.service;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;

import com.accenture.academico.model.Endereco;
import com.accenture.academico.dao.EnderecoDAO;



@ManagedBean(name = "EnderecoService")
@SessionScoped
@Service

public class EnderecoServiceImpl implements EnderecoService {

	private EnderecoDAO EnderecoDAO;

	public void setEnderecoDAO(EnderecoDAO EnderecoDAO) {
		this.EnderecoDAO = EnderecoDAO;
	}

	@Override
	//@Transactional
	public void addEndereco(Endereco e) {
		// TODO Auto-generated method stub
		EnderecoDAO.addEndereco(e);		

	}

	@Override
	public List<Endereco> listEnderecos() {
		// TODO Auto-generated method stub
		return EnderecoDAO.listEnderecos();
	}

	@Override
	//@Transactional
	public void updateEndereco(Endereco e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteEndereco(Endereco e) {
		// TODO Auto-generated method stub

	}

}
