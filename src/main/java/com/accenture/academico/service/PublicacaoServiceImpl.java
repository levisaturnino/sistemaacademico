package com.accenture.academico.service;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;

import com.accenture.academico.model.Publicacao;
import com.accenture.academico.dao.PublicacaoDAO;



@ManagedBean(name = "PublicacaoService")
@SessionScoped
@Service

public class PublicacaoServiceImpl implements PublicacaoService {

	private PublicacaoDAO PublicacaoDAO;

	public void setPublicacaoDAO(PublicacaoDAO PublicacaoDAO) {
		this.PublicacaoDAO = PublicacaoDAO;
	}

	@Override
	//@Transactional
	public void addPublicacao(Publicacao p) {
		// TODO Auto-generated method stub
		PublicacaoDAO.addPublicacao(p);		

	}

	@Override
	public List<Publicacao> listPublicacoes() {
		// TODO Auto-generated method stub
		return PublicacaoDAO.listPublicacoes();
	}

	@Override
	//@Transactional
	public void updatePublicacao(Publicacao p) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deletePublicacao(Publicacao p) {
		// TODO Auto-generated method stub

	}

}
