package com.accenture.academico.service;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;

import com.accenture.academico.model.Aluno;
import com.accenture.academico.dao.AlunoDAO;



@ManagedBean(name = "AlunoService")
@SessionScoped
@Service

public class AlunoServiceImpl implements AlunoService {

	private AlunoDAO AlunoDAO;

	public void setAlunoDAO(AlunoDAO AlunoDAO) {
		this.AlunoDAO = AlunoDAO;
	}

	@Override
	//@Transactional
	public void addAluno(Aluno a) {
		// TODO Auto-generated method stub
		AlunoDAO.addAluno(a);		

	}

	@Override
	public List<Aluno> listAlunos() {
		// TODO Auto-generated method stub
		return AlunoDAO.listAlunos();
	}

	@Override
	//@Transactional
	public void updateAluno(Aluno a) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteAluno(Aluno a) {
		// TODO Auto-generated method stub

	}

}
