package com.accenture.academico.main;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.accenture.academico.model.Pessoa;
import com.accenture.academico.util.JPAUtil;

public class PessoaTest {

	public static void main(String[] args) {

		Pessoa pessoa = new Pessoa();

		pessoa.setNome("mar");
		pessoa.setCpf("036787164584");
		pessoa.setSexo("outros");
		pessoa.setTipoPessoa("Aluno");
		//pessoa.setId(22);
		
		
		
		
		EntityManager em = JPAUtil.getEntityManager();

		/*
		 * Query q = em.createQuery("select a from Automovel a",
		 * Automovel.class); List<Automovel> autos = q.getResultList();
		 * 
		 * for(Automovel a : autos) {
		 * System.out.println("Ano de Fabricacao: "+a.getAnoFabricacao()
		 * +" - Modelo:"+a.getModelo()); }
		 */
		EntityTransaction tx = em.getTransaction();

		/*
		 * Pessoa auto = em.getReference(Pessoa.class, 2L); tx.begin();
		 * em.remove(auto); tx.commit();
		 * 
		 * 
		 * 
		 * /* Automovel auto = new Automovel(); auto.setAnoFabricacao(2010);
		 * auto.setModelo("Ferrari"); auto.setObservacoes("Nunca foi batido");
		 * 
		 * EntityTransaction tx = em.getTransaction(); tx.begin();
		 * em.persist(auto); tx.commit();
		 */

		EntityTransaction tx1 = em.getTransaction();
		tx1.begin();
		em.merge(pessoa);
		tx1.commit();
		em.close();

	}

}
