package com.accenture.academico.bean;

import javax.faces.bean.ManagedBean;
import javax.persistence.EntityManager;

import com.accenture.academico.model.Pessoa;
import com.accenture.academico.util.JPAUtil;

@ManagedBean
public class PessoaBean {

	private Pessoa pessoa = new Pessoa();

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public void salva() {
		// conseguimos a EntityManager
		EntityManager em = JPAUtil.getEntityManager();
		em.getTransaction().begin();
		em.persist(pessoa);
		em.getTransaction().commit();
		em.close();
	}
	/*
	 * public String salvar(){
	 * 
	 * System.out.println("Ola");
	 * 
	 * FacesContext context = FacesContext.getCurrentInstance();
	 * 
	 * if(!this.nome.equalsIgnoreCase(this.sobrenome)){ context.addMessage(null,
	 * new FacesMessage(FacesMessage.
	 * SEVERITY_ERROR,"Senha confirmada incorretamente","")); return "usuario";
	 * } return "sucesso"; }
	 * 
	 * public void nomeFoiDigitado() { System.out.println("\nChamou o botão " +
	 * getNome()); }
	 */

}
