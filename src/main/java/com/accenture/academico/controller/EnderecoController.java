package com.accenture.academico.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.accenture.academico.model.Endereco;
import com.accenture.academico.model.Pessoa;

@Controller
@RequestMapping("/endereco")
public class EnderecoController {

	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String index(ModelMap model){
		model.addAttribute("message","Hello Spring Web MVC!");
		return "pessoa";
	}
	
	
/*	@RequestMapping(value = "/form", method = RequestMethod.GET)
	public ModelAndView form(ModelMap model){
	
		PessoaBean pessoa = new PessoaBean();
		
		
		return new ModelAndView("pessoa","pessoa",pessoa);
	}*/
	
	   @RequestMapping(value = "/form", method = RequestMethod.GET)
	    public ModelAndView showForm() {
	        return new ModelAndView("endereco", "endereco", new Endereco());
	    }
	 
	    @RequestMapping(value = "/addendereco", method = RequestMethod.POST)
	    public String submit(@Valid @ModelAttribute("pssoa")Pessoa pessoa, 
	      BindingResult result, ModelMap model) {
	        if (result.hasErrors()) {
	            return "error";
	        }
	        model.addAttribute("cpf", pessoa.getCpf());
	        model.addAttribute("nome", pessoa.getNome());
	        model.addAttribute("tipoPessoa", pessoa.getTipoPessoa());
	        return "pessoaView";
	    }
}
