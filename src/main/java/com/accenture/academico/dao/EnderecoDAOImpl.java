package com.accenture.academico.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.accenture.academico.model.Endereco;

public class EnderecoDAOImpl implements EnderecoDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(EnderecoDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addEndereco(Endereco e) {
		// TODO Auto-generated method stub
		
		Session session = this.sessionFactory.getCurrentSession();
        session.persist(e);
        logger.info("ENDEREÇO ADICIONADO: "+e);	

	}

	@Override
	public List<Endereco> listEnderecos() {
		// TODO Auto-generated method stub

		Session session = this.sessionFactory.getCurrentSession();
        List<Endereco> EnderecosList = session.createQuery("de Endereco").list();
        for(Endereco e : EnderecosList){
            logger.info("Endereco List::"+e);
        }
        return EnderecosList;
	
	}

	@Override
	public void updateEndereco(Endereco e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteEndereco(Endereco e) {
		// TODO Auto-generated method stub

	}

}
