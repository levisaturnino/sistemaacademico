package com.accenture.academico.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.accenture.academico.model.MeioComunicacao;

public class MeioComunicacaoDAOImpl implements MeioComunicacaoDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(MeioComunicacaoDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addMeioComunicacao(MeioComunicacao m) {
		// TODO Auto-generated method stub
		
		Session session = this.sessionFactory.getCurrentSession();
        session.persist(m);
        logger.info("MEIO DE COMUNICAÇÃO ADICIONADO: "+m);	

	}

	@Override
	public List<MeioComunicacao> listMeiosComunicacao() {
		// TODO Auto-generated method stub

		Session session = this.sessionFactory.getCurrentSession();
        List<MeioComunicacao> MeiosComunicacaoList = session.createQuery("de Meio de Comunicacao").list();
        for(MeioComunicacao m : MeiosComunicacaoList){
            logger.info("MeioComunicacao List::"+m);
        }
        return MeiosComunicacaoList;
	
	}

	@Override
	public void updateMeioComunicacao(MeioComunicacao m) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteMeioComunicacao(MeioComunicacao m) {
		// TODO Auto-generated method stub

	}

}
