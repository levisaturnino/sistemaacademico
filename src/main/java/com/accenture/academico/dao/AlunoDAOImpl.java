package com.accenture.academico.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.accenture.academico.model.Aluno;

public class AlunoDAOImpl implements AlunoDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(AlunoDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addAluno(Aluno a) {
		// TODO Auto-generated method stub
		
		Session session = this.sessionFactory.getCurrentSession();
        session.persist(a);
        logger.info("ALUNO ADICIONADO: "+a);	

	}

	@Override
	public List<Aluno> listAlunos() {
		// TODO Auto-generated method stub

		Session session = this.sessionFactory.getCurrentSession();
        List<Aluno> AlunosList = session.createQuery("de Aluno").list();
        for(Aluno a : AlunosList){
            logger.info("Aluno List::"+a);
        }
        return AlunosList;
	
	}

	@Override
	public void updateAluno(Aluno a) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteAluno(Aluno a) {
		// TODO Auto-generated method stub

	}

}
