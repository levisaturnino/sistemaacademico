package com.accenture.academico.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.accenture.academico.model.Publicacao;

public class PublicacaoDAOImpl implements PublicacaoDAO{
	
	private static final Logger logger = LoggerFactory.getLogger(PublicacaoDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addPublicacao(Publicacao p) {
		// TODO Auto-generated method stub
		
		Session session = this.sessionFactory.getCurrentSession();
        session.persist(p);
        logger.info("PUBLICAÇÃO ADICIONADA: "+p);	

	}

	@Override
	public List<Publicacao> listPublicacoes() {
		// TODO Auto-generated method stub

		Session session = this.sessionFactory.getCurrentSession();
        List<Publicacao> PublicacoesList = session.createQuery("de Publicação").list();
        for(Publicacao p : PublicacoesList){
            logger.info("Publicacao List::"+p);
        }
        return PublicacoesList;
	
	}

	@Override
	public void updatePublicacao(Publicacao p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deletePublicacao(Publicacao p) {
		// TODO Auto-generated method stub
		
	}
	
	

}
