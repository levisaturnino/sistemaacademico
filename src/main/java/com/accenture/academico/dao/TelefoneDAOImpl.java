package com.accenture.academico.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.accenture.academico.model.Telefone;

public class TelefoneDAOImpl implements TelefoneDAO{

	private static final Logger logger = LoggerFactory.getLogger(TelefoneDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	@Override
	public void addTelefone(Telefone t) {
		// TODO Auto-generated method stub
		
		Session session = this.sessionFactory.getCurrentSession();
        session.persist(t);
        logger.info("TELEFONE ADICIONADO: "+t);	

	}

	@Override
	public List<Telefone> listTelefones() {
		// TODO Auto-generated method stub

		Session session = this.sessionFactory.getCurrentSession();
        List<Telefone> TelefonesList = session.createQuery("de Telefone").list();
        for(Telefone t : TelefonesList){
            logger.info("Telefone List::"+t);
        }
        return TelefonesList;
	
	}

	@Override
	public void updateTelefone(Telefone t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteTelefone(Telefone t) {
		// TODO Auto-generated method stub
		
	}

}
