package com.accenture.academico.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.accenture.academico.model.Professor;

public class ProfessorDAOImpl implements ProfessorDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(ProfessorDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addProfessor(Professor p) {
		// TODO Auto-generated method stub
		
		Session session = this.sessionFactory.getCurrentSession();
        session.persist(p);
        logger.info("PROFESSOR ADICIONADO: "+p);	

	}

	@Override
	public List<Professor> listProfessores() {
		// TODO Auto-generated method stub

		Session session = this.sessionFactory.getCurrentSession();
        List<Professor> ProfessoresList = session.createQuery("de Professor").list();
        for(Professor p : ProfessoresList){
            logger.info("Professor List::"+p);
        }
        return ProfessoresList;
	
	}

	@Override
	public void updateProfessor(Professor p) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteProfessor(Professor p) {
		// TODO Auto-generated method stub

	}

}
