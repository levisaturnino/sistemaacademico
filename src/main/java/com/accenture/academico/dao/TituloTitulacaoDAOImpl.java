package com.accenture.academico.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.accenture.academico.model.TituloTitulacao;

public class TituloTitulacaoDAOImpl implements TituloTitulacaoDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(TituloTitulacaoDAOImpl.class);

	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addTitulo(TituloTitulacao t) {
		// TODO Auto-generated method stub
		
		Session session = this.sessionFactory.getCurrentSession();
        session.persist(t);
        logger.info("TÍTULO/TITULAÇÃO ADICIONADO: "+t);	

	}

	@Override
	public List<TituloTitulacao> listTitulos() {
		// TODO Auto-generated method stub

		Session session = this.sessionFactory.getCurrentSession();
        List<TituloTitulacao> TitulosList = session.createQuery("de Titulo/Titulação").list();
        for(TituloTitulacao t : TitulosList){
            logger.info("Titulo/Titulação List::"+t);
        }
        return TitulosList;
	
	}

	@Override
	public void updateTitulo(TituloTitulacao t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteTitulo(TituloTitulacao t) {
		// TODO Auto-generated method stub
		
	}

}
