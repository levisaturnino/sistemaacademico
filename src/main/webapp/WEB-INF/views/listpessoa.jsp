<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<a href="/academicospringmvc/pessoa/form">Cadastrar de Pessoa</a><br/><br/>
<table border="2" width="70%" cellpadding="2">
	<tr>
		<th>Id</th>
		<th>Nome</th>
		<th>CPF</th>
		<th>Sexo</th>
		<th>Tipo Pessoa</th>
	</tr>
	<c:forEach var="pessoa" items="${list}">
		<tr>
			<td>${pessoa.id}</td>
			<td>${pessoa.cpf}</td>
			<td>${pessoa.nome}</td>
			<td>${pessoa.sexo}</td>
			<td>${pessoa.tipoPessoa}</td>
		</tr>
	</c:forEach>
</table>
